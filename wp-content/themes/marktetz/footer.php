<div class="contacts">
        <div class="services_description">
            <p><?php the_field('contact_field_name')?></p>
            <h2><?php the_field('contact_decr')?></h2>
            <div class="line"></div>
            <span><?php the_field('contact_slogan')?></span>
        </div>
        <ul>
            <li><img src="https://icon-library.net/images/map-png-icon/map-png-icon-12.jpg" alt=""><span><?php the_field('adress')?></span></li>
            <li><img src="https://img.icons8.com/carbon-copy/2x/phone.png" alt=""><span><?php the_field('phone_number')?></span></li>
            <li><img src="https://www.clipartwiki.com/clipimg/detail/203-2038008_email-icons-website-mail-icon-png-pink.png"
                    alt=""><span><?php the_field('email')?></span></li>
        </ul>
    </div>
    <footer>
        <div class="link_1">
            <ul>
                <li>
                    <h3>Category</h3>
                </li>
                <li><a href="">Marketing</a></li>
                <li><a href="">Branding</a></li>
                <li><a href="">SEO</a></li>
                <li><a href="">Web Design</a></li>
            </ul>
        </div>
        <div class="link_2">
            <ul>
                <li>
                    <h3>Link</h3>
                </li>
                <li><a href="">Marketing</a></li>
                <li><a href="">Branding</a></li>
                <li><a href="">SEO & SMM</a></li>
                <li><a href="">Graphic</a></li>
            </ul>
        </div>
        <div class="buttons">
            <ul>
                <li><a href="">Business</a></li>
                <li><a href="">Logo</a></li>
                <li><a href="">Video</a></li>
                <li><a href="">Instagram</a></li>
                <li><a href="">Text</a></li>
            </ul>
        </div>
        <hr>
        <span>©2019 Разрабатано в TOO Move Business Group</span>
    </footer>
</body>
</html>