<?php

add_action( 'wp_enqueue_scripts', 'style_theme');
add_action('wp_footer', 'scripts_theme');
add_action( 'after_setup_theme','theme_register_nav_menu');
add_action( 'init', 'register_post_types' );

function register_post_types(){
	register_post_type('services', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Сервисы', // основное название для типа записи
			'singular_name'      => 'Сервис', // название для одной записи этого типа
			'add_new'            => 'Добавить навык', // для добавления новой записи
			'add_new_item'       => 'Добавление навыка', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование навыка', // для редактирования типа записи
			'new_item'           => 'Новый навык', // текст новой записи
			'view_item'          => 'Смотреть навык', // для просмотра записи этого типа.
			'search_items'       => 'Искать навык в сервисах', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Сервисы', // название меню
		),
		'description'         => 'Это наши навыки в сервисах',
		'public'              => true,
		// 'publicly_queryable'  => null, // зависит от public
		// 'exclude_from_search' => null, // зависит от public
		// 'show_ui'             => null, // зависит от public
		// 'show_in_nav_menus'   => null, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		// 'show_in_admin_bar'   => null, // зависит от show_in_menu
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 4,
		'menu_icon'           => 'dashicons-format-gallery', 
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'editor' ,'author','thumbnail','excerpt'], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => [],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}


function theme_register_nav_menu(){//Регистрируем меню
	add_theme_support('title-tag');//Регистрирует title 
	add_theme_support( 'post-thumbnails', array('post','services'));//Превью только для поста и портфолио
    add_image_size(	'post_thumb',400,200,true);//Размер превью
    add_image_size( 'services', 300,150, true );
}

function style_theme(){
    wp_enqueue_style('style',get_stylesheet_uri());
    wp_enqueue_style('default', get_template_directory_uri() . '/assets/style/default.css');
    wp_enqueue_style('bootstrap-css', get_template_directory_uri().'/assets/style/bootstrap.css');
}

function scripts_theme(){
    wp_deregister_script( 'jquery-core' );//Подключаем jquery
	wp_register_script( 'jquery-core', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
    wp_enqueue_script( 'jquery' );
    
    wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/assets/scripts/bootstrap.js');
}

