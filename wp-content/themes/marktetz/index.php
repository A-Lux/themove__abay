<?php get_header(); ?>
    <div class="services">
        <div class="services_description">
            <p><?php the_field('service_name') ?></p>
            <h2><?php the_field('service_slogan') ?></h2>
            <div class="line"></div>
            <span><?php the_field('service_descr') ?></span>
        </div>
        <?php 
      // параметры по умолчанию
      $posts = get_posts( array(
         'numberposts' => 5,     
         'order' => 'ASC', 
         'post_type'   => 'services',
         'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
      ) );

      foreach( $posts as $post ){
         setup_postdata($post);
        ?>  
        <div class="services_items ">
            <?php the_post_thumbnail('services');?>
            <h3><?php the_title( ) ?></h3>
            <span><?php the_excerpt(  ); ?></span>
        </div>
        <?php
      }

      wp_reset_postdata(); // сброс
         
         ?>
    </div>
    <div class="products">
        <div class="services_description">
            <p><?php the_field('goods_name') ?></p>
            <h2><?php the_field('slogan_goods') ?></h2>
        </div>
        <div class="goods">

        <?php 
      // параметры по умолчанию
      $posts = get_posts( array(
         'numberposts' => 6,     
         'order' => 'ASC', 
         'post_type'   => 'post',
         'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
      ) );

      foreach( $posts as $post ){
         setup_postdata($post);
        ?>  
        <ul>
            <li>
                <div class="post_thumb">
                <?php the_post_thumbnail('post_thumb')?>
                </div>
                <h3><?php the_title() ?></h3>
                <p><?php the_excerpt() ?></p>
                <span>453$</span>
            </li>
        </ul>
        <?php
      }

      wp_reset_postdata(); // сброс
         
         ?>
        </div>
    </div>
    <div class="statistics">
        <ul>
            <li><span>
                    <div class="numbers"><?php the_field('stat_1') ?></div>
                </span>
                <p><?php the_field('name_stat_1')?></p>
            </li>
            <li><span>
                    <div class="numbers"><?php the_field('stat_2') ?></div>
                </span>
                <p><?php the_field('name_stat_2')?> </p>
            </li>
            <li><span>
                    <div class="numbers"><?php the_field('stat_3') ?></div>
                </span>
                <p><?php the_field('name_stat_3')?></p>
            </li>
            <li><span>
                    <div class="numbers"><?php the_field('stat_4') ?></div>
                </span>
                <p><?php the_field('name_stat_4')?></p>
            </li>
        </ul>
    </div>
    <div class="comments">
        <div class="services_description">
            <p><?php the_field('name_comment_field') ?></p>
            <h2><?php the_field('comment_decr') ?></h2>
            <div class="line"></div>
            <span><?php the_field('comment_slogan') ?></span>
        </div>
        <div class="comment">
            <span>“<?php the_field('comment') ?>”</span><br>
            <h5><?php the_field('commenter_name') ?></h5>
            <p><?php the_field('comment_photo') ?></p>
        </div>
    </div>
<?php get_footer( ) ?>