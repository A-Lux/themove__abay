<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'marketz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}1w=d}!C>k(jA]wWm^WOLS~0*BZUtTQu7x.]~0Pv+&V5deS*0O+B$Gqb*- &Q{$!' );
define( 'SECURE_AUTH_KEY',  '/tm!loP7w{;ns`E>Pjv#F>MrFXvTh0I-$UlrK@T9e7J 7g//,]=?>/?.J^8]j *D' );
define( 'LOGGED_IN_KEY',    'R{38*6&nYrInb{/$LJmM%v/^7]@!2C,^l&T&Fy8A9D T6^k=qQvx=PC9`%o?r(]I' );
define( 'NONCE_KEY',        '){v,Wk)ZTm L6w=_lN[>ErD*odr<1pKCtcIu<97c1@g-]K;>HnOmUsx?nAR<rU*{' );
define( 'AUTH_SALT',        'rCuSV/Wm[>~[/pn|7}Q;dy7%_t]/6|5%0 Coqo0l0;J#ynJ/CMX0(2^TfAWH0I!V' );
define( 'SECURE_AUTH_SALT', 'rL{*7l&Jsx+ZT^!WA[/kG|o+agqF;n4>~PWAtwmU+AWhO4%`{!ec/R#XQv]aIeeL' );
define( 'LOGGED_IN_SALT',   'n^`5^ FR1jk@#%b|1,{8,P+&HR6whG`X51J@j&U> X!2Bvq vZ,#O=cu@v:`IASk' );
define( 'NONCE_SALT',       '.w!Ib1j<2IiaY$VDZ6+*sQm~Zvh x`/iFi8Jt9?i@2pe;|8RFn*tq~LEp|{29){d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
